FROM node:12.21

WORKDIR /usr/src/app

COPY ./dist ./

RUN npm ci --only=production

EXPOSE 3001

ENV PORT=3001

CMD [ "npm", "run", "start:docker" ]