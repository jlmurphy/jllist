const { MongoClient, ObjectId } = require("mongodb");
const path = require("path");
const fs = require('fs');
const {nanoid} = require('nanoid');
const { newList } = require('../test/test-data')

require('dotenv').config({
    path: path.join(process.cwd(), `.env${!!process.env.ENV_FILE ? `.${process.env.ENV_FILE}` : ''}`)
})

const {
    MONGO_INITDB_ROOT_USERNAME,
    MONGO_INITDB_ROOT_PASSWORD,
    MONGO_INITDB_URL,
    MONGO_INITDB_PORT,
} = process.env;

let neededDbs = [
    { name: 'users', seedCollectionName: 'accounts'},
    { name: 'lists', seedCollectionName: '_meta'},
]

let MClient;

let databases = [];
let UsersDB, UsersColl;
let ListsDB, ListsColl;
let _main;

const seedDataFilter = { name: "seeding_data" };

/************************************************************************************
 *                              ADMIN LEVEL
 ***********************************************************************************/

async function getDBNames() {
    try {
        databases = await MClient.db("admin").command( { listDatabases: 1 } )
        if (!databases.databases) {
            throw 'Error - No database entry in result'
        }
        
        // Create a set of all db names
        databases.names = new Set();
        for (let db of databases.databases) databases.names.add(db.name)
        return;
    } catch(e) {
        console.log(this.name, e)
        throw e;
    }
}

async function createDatabase(name, seedCollectionName) {
    try {
        if (!name) throw 'No Name Provided For New Database';
        if (!seedCollectionName) throw 'No Name Provided for Seed Collection In Database'
        const newDB = MClient.db(name);
        await newDB.createCollection(seedCollectionName);
        return newDB;
    } catch(e) {
        console.log(`createDatabase Error -`, e)
        throw e;
    }
}

async function deleteCollection(name, fromDB) {
    try {
        if (!name) throw 'No Name Provided For Delete Collection';
        if (!fromDB) throw 'No DB Name Provided For Delete Collection'
        const newDB = MClient.db(fromDB);
        const collToDelete = newDB.collection(name);
        await collToDelete.drop();
    } catch(e) {
        console.log(`deleteCollection Error -`, e)
        throw e;
    }
}

async function checkOrSeedDbs() {
    try {
        for (let needed of neededDbs) {
            if (!databases.names.has(needed.name)) {
                try {
                    await createDatabase(needed.name, needed.seedCollectionName)    
                } catch(e) {
                    console.log(e)
                    break;
                }
            }
        }
    } catch(e) {
        console.log(`checkOrSeedDbs Error - ${e}`)
        throw e;
    }
}


/************************************************************************************
 *                              READ / FIND
 ***********************************************************************************/

/**
 * Finds an account with a filter object in the User DB's accounts collection
 * 
 * @param {*} query MongoDB filter object
 * @returns 
 */
async function findAccountBy(query) {
    try {
        if (!query) throw "Missing Query Object"
        return await UserAccs.findOne(query);
    } catch(e) {
        throw `findAccountBy Error - ${e}`;
    }
}

async function findAccountByName(name) {
    try {
        if (!name) throw 'No Name Provided'
        return findAccountBy({ name });
    } catch(e) {
        throw `findAccountByName Error - ${e}`
    }
}

async function findAccountById(id) {
    try {
        if (!id) throw 'No id Provided'
        return findAccountBy({ _id: id });
    } catch(e) {
        throw `findAccountById Error - ${e}`
    }
}

async function getSeedMetaData() {
    return await _main.findOne(seedDataFilter);
}

async function checkIfMetaDataWasSeeded() {
    try {
        const doc = await getSeedMetaData();
        return doc !== null
    } catch(e) {
        throw e;
    }
}


/************************************************************************************
 *                              WRITE / UPDATE
 ***********************************************************************************/

/**
 * Updates a document in a database's collection
 * 
 * @param {*} options 
 * @param {*} updateOptions MongoDB .updateOne updateOptions object
 * @returns 
 */
async function updateDocument(options, updateOptions) {

    try {
        if (!options) throw 'No Options Provided'
        const {
            db,
            collection,
            query,
            update,
        } = options;
        if (!db) throw 'No db Provided'
        if (!collection) throw 'No collection Provided'
        if (!query) throw 'No query Provided'
        if (!update) throw 'No update Provided'
        return await MClient.db(db).collection(collection).updateOne(
            query, 
            !updateOptions ? { upsert: true} : updateOptions
        );
    } catch(e) {
        throw `updateDocument Error - ${e}`
    }
}

/**
 * Inserts the lists from a given account into a dedicated collection
 * 
 * 
 * @param {string} account object of the account from the db file
 * @returns {string} collection name (NANOID)
 */
async function seedlistsCollectionForAccount(account) {
    try {
        const listsCollectionId = nanoid();

        const coll = await ListsDB.createCollection(listsCollectionId);

        if (!account.lists) throw "Invalid Account for ${account.name}, no lists object";

        for (let list of account.lists) {
            try {
                if (list.items.length === 0) list.items = newList.withGoodName.items;
                for (let i = 0; i < list.items.length; i++) {
                    list.items[i]._id = new ObjectId()
                }
                await coll.insertOne(list);
            } catch(e) {
                console.log(e);
                throw e
            }
        }

        return listsCollectionId;
    } catch(e) {
        throw `seedlistsCollectionForAccount Error - ${e}`;
    }
}

/**
 * Injects an account object in the users' DB's accounts collection
 * 
 * @param {*} object single account object
 * @returns 
 */
async function insertAccount(object) {
    try {
        if (!object) throw "Missing Insert Object"
        const collectionID = await seedlistsCollectionForAccount(object);
        return await UserAccs.insertOne({
            name: object.name,
            list_collections: collectionID
        });
    } catch(e) {
        throw `findAccountBy Error - ${e}`;
    }
}

/**
 * Updates the metadata Document to flag the db as seeded
 */
async function updateSeedMetaData() {
    try {
        const doc = await getSeedMetaData();
        const newValue = { ...seedDataFilter , isSet: true };
        let updateResult;
        if (!doc) {
            updateResult = await _main.insertOne(newValue);
        } else {
            updateResult = await _main.findOneAndUpdate(seedDataFilter, {
                '$set': {
                    isSet: true
                }
            }, { upsert: true });
        }
        console.log(
            `${updateResult.matchedCount} document(s) matched the filter, updated ${updateResult.modifiedCount} document(s)`,
          );
    } catch(e) {
        throw e;
    }
}


/************************************************************************************
 *                              DELETES
 ***********************************************************************************/

/**
 * Flushes the users and lists databases
 * 
 * @returns 
 */
 async function flushDBS() {
    try {
        // First clear all list collections
        
        // Get the collecion names from the lists database
        let listCNames = await (
            await ListsDB.listCollections(
                {}, 
                {mameOnly: true}
            ).toArray()
        )
        // Filter out the names only
        listCNames = await listCNames.map((coll) => {
            return !!coll ? coll.name : 'undefined'
        });

        /**
         * Loop through the list of collection names
         * Delete each collection in the lists db
         */
        for (let collection of listCNames) {
            try {
                await deleteCollection(collection, 'lists');
            } catch(e) {
                console.log(e);
                break;
            }
        }

        // Drop the whole accounts collection
        await UsersDB.collection('accounts').drop();

        // Re-assign the variable for good measure
        UserAccs = await UsersDB.createCollection('accounts');

        return;
    } catch(e) {
        console.log(`createDatabase Error -`, e)
        throw e;
    }
}


/**
 *      SEEDING
 */

/**
 * Seeds the users and lists DBs from the db.json file
 * 
 * @returns 
 */
async function seedContent() {
    try {

        // Skip if the metadata exists
        if (await checkIfMetaDataWasSeeded()) return;

        const dbfile = await fs.readFileSync(
            path.join(
                process.cwd(), 
                './seeds/db.json'
            ));
        const db = await JSON.parse(dbfile);

        if (!db.db) throw "Invalid db.json file type - no top-level 'db' key";

        for (let account of db.db) {
            
            let c = await findAccountByName(account.name);
            if (!c || c === null) {
                c = await insertAccount(account)
            }
        }
        await updateSeedMetaData();
        
        console.log('Seeded databases')
    } catch(e) {
        throw e;
    }
}

/**
 *      START
 */

async function run() {
    try {
        MClient = await new MongoClient(
            `mongodb://${MONGO_INITDB_ROOT_USERNAME}:${MONGO_INITDB_ROOT_PASSWORD}` + 
                `@${MONGO_INITDB_URL}${!MONGO_INITDB_PORT ? ':27017' : ''}`,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }
        );

        // Connect the client to the server
        await MClient.connect();

        // Establish and verify connection
        await MClient.db("admin").command({ ping: 1 });

        console.log("Connected successfully to server");

        // Singletons
        UsersDB = MClient.db('users');
        UserAccs = UsersDB.collection('accounts');
        ListsDB = MClient.db('lists');
        _main = MClient.db('lists').collection('_meta')
        
        // IF the FLUSH_DB env flag is set to yes, flush the databases


        if (process.env.FLUSH_DBS === 'yes' || process.env.FLUSH_DBS_ONLY === 'yes') {
            await flushDBS();
            console.log("Flushed the Databases")
        }
        
        if (process.env.FLUSH_DBS_ONLY === 'yes') return;

        //Check dbs, seed them
        await getDBNames()

        // Create the Databases
        await checkOrSeedDbs()

        // Seed content into the DBs
        await seedContent();
        
    } catch(e) {
        console.log(e)  
    } finally {
        // Ensures that the client will close when you finish/error
        await MClient.close();
    }
}


run().catch(console.dir);