
const {
    DB_ENGINE
} = process.env;

(async () => {
    if (!DB_ENGINE) throw "No DBENGINE specified";
    switch (DB_ENGINE) {
        case 'mongo':
            require('./populate')
            break;
        case 'psql':
            require('./populate-pg')
            break;
        default:
            console.error(`${process.env.DB_ENGINE} is not a valid value for DB_ENGINE`)
            process.exit(1);
    }
})()