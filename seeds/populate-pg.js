const { Pool, Client } = require('pg');
const path = require('path')
const pgt = require('pgtools');
const util = require('util');
const {ObjectId} = require('mongodb');
const {nanoid} = require('nanoid');
const fs = require('fs')
const { newList } = require('../test/test-data')

require('dotenv').config({
    path: path.join(process.cwd(), `.env${!!process.env.ENV_FILE ? `.${process.env.ENV_FILE}` : ''}`)
})


const config = {
    host: 'localhost',
    port: '5432',
    user: 'root',
    password: 'example',
}

const pool = new Pool();

const getAllTableNames = async () => {
    try {
        const allNames = `SELECT table_name
        FROM information_schema.tables
        WHERE table_type='BASE TABLE'
        AND table_schema='public';`
        const result = await pool.query(allNames)
        return result.rows.map(item => item.table_name )
    } catch (e) {
        throw e;
    }
}

const dropDB = async () => {
    try {
        let allNames = await getAllTableNames()
        if (allNames.length === 0) return;
        allNames = await allNames.join(', ');
        allNames += ';'
        return await pool.query(`DROP TABLE ${allNames}`);
    } catch (e) {
        throw e;
    }
}

const flushDBS = dropDB;

/**
 * 
 * @param {*} name name of the table
 * @param {*} columns list of CREATE TABLE fields/properties
 * @returns 
 */
const createTable = async (name, columns) => {
    try {
        if (!name) throw 'No Table Name Specified'
        if (!columns) throw 'No Table Columns Specified'
        let query = `CREATE TABLE ${name} (`;

        /**
         * Builds the CREATE TABLE query
         */
        for (let i = 0 ; i < columns.length; i++) {
            const {
                name,
                type,
                constraint
            } = columns[i];
            if (i > 0) query += ','
            query += `\n ${name}`
            if (!!type) query += ` ${type}`
            if (!!constraint) query += ` ${constraint}`
        }
        query += "\n)"
        return await pool.query(query);
    } catch(e) {
        throw e;
    }
}

const updateSeedMetaData = async () => {
    try {
        const seeded = await isDatabaseSeeded()
        if (seeded.result === false) {
            await pool.query(`CREATE TABLE _meta (
                key VARCHAR PRIMARY KEY,
                state VARCHAR NOT NULL
            )`);
            return await pool.query(`INSERT INTO _meta (key, state) VALUES('seeding_data', 'true')`)
        }
    } catch(e) {
        throw e;
    }
}



const getMetaTable = async (condition) => {
    try {
        const query = !!condition ? `SELECT key, state FROM _meta WHERE ${condition}` : `SELECT * FROM _meta`
        return await pool.query(query)
    } catch(e) {
        throw e;
    }
}   


const isDatabaseSeeded = async () => {
    try {
        const names = await getAllTableNames()
        // console.log(names)
        if (names.length < 1) return {result: false};
        const namesSet = new Set(names);
        if (!namesSet.has('_meta')) return {result: false};
        const result = await getMetaTable(`key='seeding_data'`);
        return { ...result.rows[0], result: result.rows[0].state === 'true' }
    } catch(e) {
        throw e;
    }
}

const addList = async (collectionId, list) => {
    try {
        if (!collectionId) throw "No Collection ID Specified";
        if (!list) throw "No Collection ID Specified";
        const {
            _id,
            name,
            description,
            items
        } = list
        if (!name) throw "No name specified for list";
        if (name.length > 96) throw "List name is too long"
        /**
         * Should be present
         */
        if (!items) throw "No items specified for list";
        let keys = "(_id, name";
        let values = "(";
        values += `'${_id}'`
        values += `, '${name.replace(`'`, `''`)}'`
        if (!!description) {
            values += `, '${description.replace(`'`, `''`)}'`
            keys += ', description';
        }
        keys += ', items';
        keys += ")";
        values += await `, '`
        values += await `${JSON.stringify(JSON.parse(JSON.stringify(items)))}`.replace(`'`, `''`)
        values += await `'`
        values += ")";
        const query = `INSERT INTO _${collectionId} ${keys} VALUES ${values}`;
        await pool.query(query)
    } catch(e) {
        throw e;
    }
}

const addUser = async (user) => {
    try {

        /**
         * We use mongodb objectId's to maintain cross-storage compatibility
         */
        const userId = `${(new ObjectId()).toString()}`;
        const listsTableID = `${(new ObjectId()).toString()}`;
        // const listsTableID = `_${nanoid()}`;

        /**
         * Create user profile object
         */

        const newUser = {
            _id: userId,
            name: user.name,
            list_collections: listsTableID
        }

        /**
         * Create user's lists table
         */
        await createTable(`_${listsTableID}`,[
            {
                name: "_id",
                type: "VARCHAR PRIMARY KEY" 
            },
            {
                name: "name",
                type: "VARCHAR(96) NOT NULL" 
            },
            {
                name: "description",
                type: "VARCHAR" 
            },
            {
                name: "items",
                type: "jsonb" 
            }
        ]);
        /**
         * IF Any lists provided, seed them. 
         * */
        if (!!user.lists) {
            for (let i = 0; i < user.lists.length; i++) {
                user.lists[i]._id = `${(new ObjectId()).toString()}`;
                if (user.lists[i].items.length === 0) user.lists[i].items = newList.withGoodName.items;
                for (let y = 0; y < user.lists[i].items.length; y++) {
                    user.lists[i].items[y]._id = `${(new ObjectId()).toString()}`;
                }
                await addList(listsTableID, user.lists[i])
            }
            
        } else {
            user.lists = []
        }

        /**
         * Add the user account
         */
        const query = `INSERT INTO users (_id, name, list_collections) 
        VALUES('${newUser._id}', '${newUser.name}', '${newUser.list_collections}')`;
        return await pool.query(query)
    } catch(e) {
        throw e;
    }
}

const seedContent = async () => {
    try {
        const dbfile = await fs.readFileSync(
            path.join(
                process.cwd(), 
                './seeds/db.json'
            ));
        
        const db = await JSON.parse(dbfile);
        if (!db.db) throw "Invalid db.json file type - no top-level 'db' key";
        
        for (let account of db.db) {
            await addUser(account);
        }

        await updateSeedMetaData();
    } catch(e) {
        throw e;
    }
}

const run = async () => {
    try {
        // const ok = await isDatabaseSeeded()
        if (process.env.FLUSH_DBS === 'yes' || process.env.FLUSH_DBS_ONLY === 'yes') {
            await flushDBS();
            console.log("Flushed the Databases")
        }
        await createTable('users', [
            {
                name: "_id",
                type: "VARCHAR PRIMARY KEY" 
            },
            {
                name: "name",
                type: "VARCHAR UNIQUE NOT NULL" 
            },
            {
                name: "list_collections",
                type: "VARCHAR NOT NULL" 
            }
        ])
        await seedContent();
        await updateSeedMetaData()
        console.log("Seeded PostgeSQL database")
    } catch(e) {
        console.log(e);
    }
}

run()
    .then(() => process.exit(0))
    .catch((e) => {
        console.log(e)
        process.exit(1)
    })