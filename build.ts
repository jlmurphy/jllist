/**
 * Remove old files, copy front-end ones.
 */

import fs from 'fs-extra';
import Logger from 'jet-logger';
import path from 'path';
import { exec } from './spec/cli';

// Setup logger
const logger = new Logger();
logger.timestamp = false;

(async () => {
    try {
        // Create or remove current build
        await createDistIfNotPresent()
        await remove('./dist/');
        // Copy production env file
        /**
         */
        // Copy back-end files
        await exec('tsc --build tsconfig.json', './')
        switch (process.env.NODE_ENV) {
            case 'dev' || 'development':
                await copy('./.env.dev', './dist/.env');
                break;
            case 'prod' || 'production':
                await copy('./.env.prod', './dist/.env');
                break;
        }
        await copy('./seeds', './dist/seeds');
        await copy('./test/test-data.js', './dist/test/test-data.js');
        await createDistPackageJson();
    } catch (err) {
        logger.err(err);
    }
})();

function createDistIfNotPresent() {
    return new Promise((res, rej) => {
        return fs.pathExists('./dist', (err) => {
            if (!!err) {
                return (
                    exec('mkdir dist', './')
                    .then(res)
                    .catch(rej)
                );
            }
            return (res("ok"));
        })
    });
}

async function createDistPackageJson() {
    try {
        let currentJSON;
        let newJSONString;
        let currentJSONBuff = await fs.readFileSync(path.join(process.cwd(), 'package.json'), 'utf-8');
        // let currentJSONString = await JSON.stringify(currentJSONBuff);
        // console.log(currentJSONString)
        currentJSON = await JSON.parse(currentJSONBuff);
        let newJSON = { 
            dependencies: currentJSON.dependencies,
            scripts: {
                "seed": "node ./seeds/index.js",
                start: 'NODE_ENV=prod node ./src/server.js',
                "start:docker": "ls && npm run seed && npm run start"
            },
        }
        newJSONString = await JSON.stringify(newJSON);
        await fs.writeFileSync(path.join(process.cwd(), './dist/package.json'), newJSONString);
    } catch(e) {

    }
}

function remove(loc: string): Promise<void> {
    return new Promise((res, rej) => {
        return fs.remove(loc, (err) => {
            return (!!err ? rej(err) : res());
        });
    });
}


function copy(src: string, dest: string): Promise<void> {
    return new Promise((res, rej) => {
        return fs.copy(src, dest, (err) => {
            return (!!err ? rej(err) : res());
        });
    });
}


// function exec(cmd: string, loc: string): Promise<void> {
//     return new Promise((res, rej) => {
//         return childProcess.exec(cmd, {cwd: loc}, (err, stdout, stderr) => {
//             if (!!stdout) {
//                 logger.info(stdout);
//             }
//             if (!!stderr) {
//                 logger.warn(stderr);
//             }
//             return (!!err ? rej(err) : res());
//         });
//     });
// }
