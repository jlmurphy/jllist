const request = require('request');
const { assert, expect } = require('chai');
const { newList, newListItem } = require('./test-data');

const port = () => {
    return process.env.DB_ENGINE === 'mongo' ? '3000' : '3001';
}

const baseUrl = () => `http://localhost:${port()}/list`

const baseUrlUsers = () => `http://localhost:${port()}/users`

let userName = 'Minda'

const bearer = () => `Bearer: ${userName}`

const headers = () => {
    return {
        'Content-Type': 'application/json',
        'Authorization' : `Bearer: ${userName}`
    }
}

const checkErrResp = (err, resp, expectedCode) => {
    assert(!err, err);
    expect(resp.statusCode).to.equal(expectedCode)
}

const checkExpectedErrResp = (err, resp, expectedCode) => {
    assert(err === null, "No Error Returned");
    expect(resp.statusCode).to.equal(expectedCode)
}

const checkResponseBody = (body, hasData) => {
    const bod = typeof body === 'string' ? JSON.parse(body) : body;
    assert(bod.code !== undefined, 'Body code key does not exist')
    assert(bod.message !== undefined, 'Body message key does not exist')
    if (hasData !== undefined && hasData === true) {
        assert(bod.data !== undefined, 'Body data key does not exist');
    }
    return bod;
}

/**
 * Returns the Request options to update a list
 * 
 * @param {string} listId id of the list
 * @returns 
 */
const updateListNameRequestOptions = (listId) => {
    return { 
        headers: { ...headers() }, 
        method: 'POST',
        body:  { name: `This Was Something ${Math.random()}` },
        json: true,
        url: `${baseUrl()}/${listId}`,
    }
}

/**
 * Returns the Request options to update a list with an invalid name
 * 
 * @param {string} listId id of the list
 * @returns 
 */
const updateListBadNameRequestOptions = (listId) => {
    return { 
        headers: { ...headers() }, 
        method: 'POST',
        body:  { name: `${newList.withNameTooLong.name} ${Math.random()}` },
        json: true,
        url: `${baseUrl()}/${listId}`,
    }
}

const addAListRequestOptions = () => {
    return { 
        headers: { ...headers(), method: 'POST' }, 
        method: 'POST',
        body:  newList.withGoodName,
        json: true,
        url: `${baseUrl()}`,
    }
}

const addAListInvalidNameRequestOptions = () => {
    return { 
        headers: { ...headers() }, 
        method: 'POST',
        body:  newList.withNameTooLong,
        json: true,
        url: `${baseUrl()}`,
    }
}

const getUsers = (cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrlUsers()}`,
            method: 'GET'
        }, 
        cb
    )
}

const getList = (listID, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}`,
            method: 'GET'
        }, 
        cb
    )
}

/**
 * ITEMS METHODS
 */

 const getListItems = (listID, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items`,
            method: 'GET'
        }, 
        cb
    )
}

const getListItem = (listID, itemId, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items/${itemId}`,
            method: 'GET'
        }, 
        cb
    )
}

const insertNewListItem = (listID, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items`,
            body: newListItem.newItem,
            json: true,
            method: 'POST'
        }, 
        cb
    )
}

const insertNewListItemWithBadDescription = (listID, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items`,
            body: newListItem.newItemWithDescriptionTooLong,
            json: true,
            method: 'POST'
        }, 
        cb
    )
}

const updateListItem = (listID, itemId, content, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items/${itemId}`,
            body: content,
            json: true,
            method: 'POST'
        }, 
        cb
    )
}

const updateListItemName = (listID, itemId, cb) => {
    updateListItem(
        listID,
        itemId,
        newListItem.withGoodDescription,
        cb
    )
}


const updateListItemWithBadName = (listID, itemId, cb) => {
    updateListItem(
        listID,
        itemId,
        newListItem.withDescriptionTooLong,
        cb
    )
}


const updateListItemDoneFlag = (listID, itemId, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items/${itemId}/checked`,
            method: 'GET'
        }, 
        cb
    )
}

const deleteListItem = (listID, itemId, cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}/${listID}/items/${itemId}`,
            method: 'DELETE'
        }, 
        cb
    )
}

const refreshUser = (cb) => {
    getUsers((err, resp, body) => {
        checkErrResp(err, resp, 200);
        const { data } = checkResponseBody(body)
        assert(Array.isArray(data), 'Body data key is not an array');
        const newUserIndex = Math.floor(Math.random() * data.length * 0.99)
        const newUser = data[newUserIndex];
        if (newUser === undefined || newUser.name === undefined) {
            throw `Invalid user ${newUser.name} at ${index}`;
        }
        userName = newUser.name;
        cb();
    });
}

/**
 * Get a user's lists as an array
 * @param {} cb callback from request object
 */
const getListsFor = (cb) => {
    request(
        { 
            headers: { ...headers() },
            url: `${baseUrl()}s`,
        }, 
        (err, resp, body) => {
            checkErrResp(err, resp, 200);
            const bod = checkResponseBody(body)
            assert(Array.isArray(bod.data), 'Body data key is not an array');
            cb(err, resp, bod)
        }
    )
}

/**
 * 
 * @param {(err, resp, body, list, index) => void} cb 
 */
const getRandomListFromUser = (cb) => {
    /**
     * We refresh the user each time so as to avoid
     * any mixup in the data
     */
    refreshUser(() => {
        getListsFor((err, resp, { data }) => {
            if (data.length === 0) throw `User has no lists`;
            /**
             * .floor, with .rand * 0.99, should also guarantee
             * we do not exceed the max bound of the list
             */
            const index = Math.floor(Math.random() * 0.99 * data.length);
            return cb(null, resp, data[index], data, index);
        });
    })
}

const getSingleListFor = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string')
            cb()
        }
    )
}

const performAddAList = (body1, cb) => {
    request(
        addAListRequestOptions(), 
        (err, resp, body) => {
            checkErrResp(err, resp, 200);
            /**      
             * We now check that the user has +1 lists
             */
            getListsFor((err2, resp2, body2) => {
                const preBod = checkResponseBody(body1);
                assert(Array.isArray(preBod.data), 'postBod data key is not an array');
                assert(preBod.data.length === (body2.data.length - 1), 'list length is not +1');
                cb()
            })
        }
    )
}

const addAListToUser = (cb) => {
    getListsFor((err1, resp1, body1) => {
        checkErrResp(err1, resp1, 200);
        performAddAList(body1, cb)
    });
}

const addAListWithInvalidNameToUser = (cb) => {
    request(
        addAListInvalidNameRequestOptions(), 
        (err, resp, body) => {
            checkExpectedErrResp(err, resp, 400);
            cb();
    })
}

const checkNewListAgainstPrevious = (body, cb) => {
    getList(body._id, (err2, resp2, body2) => {
        checkErrResp(err2, resp2, 200);
        const {
            data,
        } = JSON.parse(body2);
        assert(
            !Array.isArray(data), 
            `getlist data is an array, should be single object`
        );
        expect(data.name).not.to.equal(body.name);
        cb();
    })
}

const updateListName = (opts, cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            request(
                opts, 
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    checkNewListAgainstPrevious(body, cb);
                }
            )
        }
    )
}

const pickListAndUpdateName = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            request(
                updateListNameRequestOptions(body._id),
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    checkNewListAgainstPrevious(body, cb);
                }
            )
        }
    )
}

const pickListAndUpdateWithBadName = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            request(
                updateListBadNameRequestOptions(body._id),
                (err1, resp1, body1) => {
                    checkExpectedErrResp(err1, resp1, 400);
                    cb()
                }
            )
        }
    )
}

/**
 * ITEMS
 */

const extractRandomItemFrom = (list) => {
    if (list.items.length < 1) throw `List ${list.name} has no items - If this error occurs, rerun the tests!`
    return list.items[Math.floor(Math.random() * list.items.length * 0.99)];
}

const getsAllItemsOfList = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            getListItems(
                body._id, 
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    const bod = checkResponseBody(body1, true)
                    assert(Array.isArray(bod.data), 'Body data key is not an array');
                    // checkNewListAgainstPrevious(body, cb);
                    expect(body.items.length).to.equal(bod.data.length)
                    cb();
                }
            )
        }
    )
}

const getsAnItemOfList = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            const item = extractRandomItemFrom(body)
            getListItem(
                body._id,
                item._id,
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    const bod = checkResponseBody(body1, true)
                    assert(item._id === bod.data._id, 'Body data key is not an array');
                    // checkNewListAgainstPrevious(body, cb);
                    cb();
                }
            )
        }
    )
}

const createsAnItemOfList = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            insertNewListItem(
                body._id,
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    const bod = checkResponseBody(body1, true)
                    assert(!Array.isArray(bod.data), 'Body data key is not an array');
                    assert(Array.isArray(bod.data.items), 'Body data.items key is not an array');
                    expect(body.items.length).to.equal(bod.data.items.length - 1)
                    cb();
                }
            )
        }
    )
}

const updatesAnItemDescriptionOfList = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            const item = extractRandomItemFrom(body)
            updateListItemName(
                body._id,
                item._id,
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    const { data } = checkResponseBody(body1, true)
                    assert(!Array.isArray(data), 'Body data key is not an array');
                    assert(Array.isArray(data.items), 'Body data.items key is not an array');
                    expect(body.items.length).to.equal(data.items.length)
                    for (let fItem of data.items) {
                        if (item._id === fItem._id) {
                            expect(item.description).to.not.equal(fItem.description)
                            cb();
                            return;
                        }
                    }
                    throw "Updated item not found in returned list - should be present"
                }
            )
        }
    )
}

const updatesAnItemOfListWithBadDescription = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            const item = extractRandomItemFrom(body)
            updateListItemWithBadName(
                body._id,
                item._id,
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 400);
                    cb();
                }
            )
        }
    )
}

const updatesAnItemDoneFlagOfList = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            const item = extractRandomItemFrom(body)
            updateListItemDoneFlag(
                body._id,
                item._id,
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    const { data } = checkResponseBody(body1, true)
                    assert(!Array.isArray(data), 'Body data key is not an array');
                    assert(Array.isArray(data.items), 'Body data.items key is not an array');
                    expect(body.items.length).to.equal(data.items.length);
                    for (let fItem of data.items) {
                        if (item._id === fItem._id) {
                            expect(fItem.done).to.equal(item.done + 1)
                            cb();
                            return;
                        }
                    }
                    throw "Updated item not found in returned list - should be present"
                }
            )
        }
    )
}

const deletesAnItemFromAList = (cb) => {
    getRandomListFromUser( 
        (err, resp, body, list, index) => {
            checkErrResp(err, resp, 200);
            assert(typeof body._id === 'string');
            const item = extractRandomItemFrom(body)
            deleteListItem(
                body._id,
                item._id,
                (err1, resp1, body1) => {
                    checkErrResp(err1, resp1, 200);
                    const { data } = checkResponseBody(body1, true)
                    assert(!Array.isArray(data), 'Body data key is not an array');
                    assert(Array.isArray(data.items), 'Body data.items key is not an array');
                    expect(body.items.length).to.equal(data.items.length + 1);
                    for (let fItem of data.items) {
                        if (item._id === fItem._id) throw "Updated item found in returned list - should not be present"
                    }
                    cb();
                }
            )
        }
    )
}



module.exports = {
    baseUrl,
    userName,
    headers,
    getList,
    getListsFor,
    getSingleListFor,
    getRandomListFromUser,
    checkResponseBody,
    checkErrResp,
    checkExpectedErrResp,
    updateListNameRequestOptions,
    updateListBadNameRequestOptions,
    addAListToUser,
    addAListWithInvalidNameToUser,
    addAListInvalidNameRequestOptions,
    addAListRequestOptions,
    checkNewListAgainstPrevious,
    pickListAndUpdateName,
    pickListAndUpdateWithBadName,
    getsAllItemsOfList,
    getsAnItemOfList,
    createsAnItemOfList,
    updatesAnItemDescriptionOfList,
    updatesAnItemOfListWithBadDescription,
    updatesAnItemDoneFlagOfList,
    deletesAnItemFromAList
}