module.exports.newList = {
    withGoodName: {
        "name": "Visit Toronto",
        "description": "My todo list for toronto",
        "items": [
            {
                "name": "Visit the CN Tower",
                "done": 1,
                "description": "Go to the CN Tower and take pictures"
            },
            {
                "name": "Go see a Jay's game",
                "done": 1,
                "description": "Say hi to the coach"
            }
        ]
    },
    withNameTooLong: {
        "name": "dSmqYOqWSndrxJ56AMeszElcHC35ZmgJeDjQlMwLTEq1mjrr7fSV2zzw1aggJs6r0Wg0kNyWMlyF9w3zoQQJRCadTWtY00000009l",
        "description": "My todo list for toronto",
        "items": [
            {
                "name": "Visit the CN Tower",
                "done": 1,
                "description": "Go to the CN Tower and take pictures"
            },
            {
                "name": "Go see a Jay's game",
                "done": 1,
                "description": "Say hi to the coach"
            }
        ]
    }
}
module.exports.newListItem = {
    newItem: {
        "name": "Visit the NBA",
        "description": "Visiting Some NBA"
    },
    newItemWithDescriptionTooLong: {
        "name": "Visit the NBA",
        "description": "i8lQI1vIwV5sUohe5fgpZD6nCre3uzv0ek1L9CWSKZcBsbXY6NAx2ZoQPrNIHJVcFeR5Mlgvhlr5JV2XXIIYNQ4KuZnX7mTCwevDStUVxpoZL1kSSFex5CVeF7WZdVC1OZYfnyUWYh1X3iDG2bUmwS5Vxxfac9tNOH1u57CrOTr7uMcI1Pr7Ofsmnse64Gq33HPNfurCP3D4e3rLsGlC4KJDDjIyhZdvNajrVyBA1t97cUvqgrJ3u1TWbeGLNIn0p"
    },
    withGoodDescription: {
        "name": "Visit the CN Tower",
        "description": "What a whacky description!"
    },
    withDescriptionTooLong: {
        "name": "Visit the CN Tower",
        "description": "i8lQI1vIwV5sUohe5fgpZD6nCre3uzv0ek1L9CWSKZcBsbXY6NAx2ZoQPrNIHJVcFeR5Mlgvhlr5JV2XXIIYNQ4KuZnX7mTCwevDStUVxpoZL1kSSFex5CVeF7WZdVC1OZYfnyUWYh1X3iDG2bUmwS5Vxxfac9tNOH1u57CrOTr7uMcI1Pr7Ofsmnse64Gq33HPNfurCP3D4e3rLsGlC4KJDDjIyhZdvNajrVyBA1t97cUvqgrJ3u1TWbeGLNIn0p"
    },
}