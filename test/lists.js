const {
    getListsFor,
    getSingleListFor,
    addAListToUser,
    addAListWithInvalidNameToUser,
    pickListAndUpdateName,
    pickListAndUpdateWithBadName,
    getsAllItemsOfList,
    getsAnItemOfList,
    createsAnItemOfList,
    updatesAnItemDescriptionOfList,
    updatesAnItemOfListWithBadDescription,
    updatesAnItemDoneFlagOfList,
    deletesAnItemFromAList
} = require('./methods');

describe('List actions', () => {

    it('gets all lists', (done) => {
        getListsFor(done);
    });

    it('gets a single list for user', (done) => {
        getSingleListFor(done);
    });

    it('adds a list', (done) => {
        addAListToUser(done);
    });

    it('can change the name of a list', (done) => {
        pickListAndUpdateName(done);
    })
    
    it('cannot add a list with a name longer than 96 characters', (done) => {
        addAListWithInvalidNameToUser(done);
    });

    it('fails to change a list name due to the new name length', (done) => {
        pickListAndUpdateWithBadName(done);
    });
});

describe('Item actions', () => {

    it('gets all items', (done) => {
        getsAllItemsOfList(done);
    });

    it('gets an item', (done) => {
        getsAnItemOfList(done);
    });

    it('creates an item in a list', (done) => {
        createsAnItemOfList(done);
    });

    it('updates an item description in a list', (done) => {
        updatesAnItemDescriptionOfList(done);
    });
    
    it('fails to update an item in a list with a bad description', (done) => {
        updatesAnItemOfListWithBadDescription(done);
    });
    
    it('toggle the done flag of an item', (done) => {
        updatesAnItemDoneFlagOfList(done);
    });

    it('deletes an item from a list', (done) => {
        deletesAnItemFromAList(done);
    });
});