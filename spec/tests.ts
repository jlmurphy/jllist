import { exec } from "./cli";


/**
 * TODO:
 * - launch child process to launch application
 * - 
 */

(async () => {
    console.log(process.cwd())
    try {
        console.log("sEEDING DATABASE ", process.env.DB_ENGINE)
        if (!process.env.DB_ENGINE) throw "No value specified for DB_ENGINE";
        switch (process.env.DB_ENGINE) {
            case 'mongo':
                require('./populate')
                break;
            case 'psql':
                require('./populate-pg')
                break;
            default:
                console.error(`${process.env.DB_ENGINE} is not a valid value for DB_ENGINE`)
                process.exit(1);
        }
        console.log("BEGINING TESTS FOR ", process.env.DB_ENGINE)
        await exec(`npm run tests:env:${process.env.DB_ENGINE}`, process.cwd());
        process.exit(0)
    } catch(e) {
        console.error(e)
        process.exit(1);
    }
})()