# !/bin/bash

echo "Starting build.."
echo $PWD

npm ci 

npm run build:publish

cd dist && npm install --only=prod

cd ..

echo "Finished building... uploading artifacts"