import './pre-start';
import app from './app';

// Start the server
const port = Number(process.env.PORT || 3000);
const host = Number(process.env.APP_HOST || 'localhost');
// @ts-ignore
app.listen(port, host, () => {
    console.log(`Server listening on ${port}`)
});
