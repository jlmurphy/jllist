import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import path from 'path';
import helmet from 'helmet';

import express, { NextFunction, Request, Response } from 'express';
import StatusCodes from 'http-status-codes';
import 'express-async-errors';

import API from './routes';

const app = express();
const { BAD_REQUEST } = StatusCodes;



/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());

// Show routes called in console during development
if (process.env.NODE_ENV === 'development' || 'dev') {
    app.use(morgan('dev'));
}

// Security
if (process.env.NODE_ENV === 'production' || 'prod') {
    app.use(helmet());
}

// Add APIs
app.use(API);

// Print API errors
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    // logger.err(err, true);
    return res.status(BAD_REQUEST).json({
        error: err.message,
    });
});

export default app;