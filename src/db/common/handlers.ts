
export const checkIfUserIdExists = (req: Request): string => {
    try {
        if (!req.headers) throw 'No userId provided';
        const {
            // @ts-ignore
            userId
        } = req.headers;
        return String(userId);
    } catch(e) {
        throw e;
    }
}

/**
 * Checks is any of the following are defined
 * @param user 
 * @param list 
 * @param item 
 */
export const checkUserPassedInRequest = (user: string) => {
    try {
        if (!user) throw 'No userId provided';
    } catch(e) {
        throw e;
    }
}

export const checkUserListPassedInRequest = (user: string, list: string) => {
    try {
        checkUserPassedInRequest(user)
        if (!list) throw 'No listId provided';
    } catch(e) {
        throw e;
    }
}

/**
 * Checks is any of the following are defined
 * @param user 
 * @param list 
 * @param item 
 */
export const checkValuesPassedInRequest = (user: string, list: string, item?: string) => {
    try {
        checkUserListPassedInRequest(user, list)
        if (item !== undefined && typeof item !== 'string') throw 'No itemId string provided';
    } catch(e) {
        throw e;
    }
}


/**
 * Checks is any of the following are defined
 * @param user 
 * @param list 
 * @param {any} content 
 */
 export const checkMainValuesPassedInRequestWithOpenThird = (user: string, list: string, other?: any) => {
    try {
        checkUserPassedInRequest(user);
        if (!list) throw 'No listId provided';
        // if (other !== undefined) throw 'No third value string provided';
    } catch(e) {
        throw e;
    }
}
