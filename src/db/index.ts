import IListDBClient from "src/types/dbclient";
import MongoDB from "./mongo";
import { 
    IAllUserLists, 
    IDBList, 
    IDBListItem, 
    INewDBList, 
    INewDBListItem, 
    IUpdateDBListFields, 
    IUpdateDBListItem 
} from "src/types/List";
import User from "src/types/User";
import PostgreSQL from "./psql";

/************************************************************************************
 *                              Database Client Engine
 * 
 *   Responsible for switching between Mongo and PostgreSQL along SOLID principles
 *   
 *   Each method of the Engine mirrors the methods in the IListDBClient interace
 *   adopted by the Database Driver clients classes in the mongo an psql folders
 * 
 ***********************************************************************************/


class DBEngine {

    _client: IListDBClient;

    constructor() {

        switch (process.env.DB_ENGINE) {
            case 'mongo':
                this._client = new MongoDB();
                break;
            case 'psql':
                this._client = new PostgreSQL();
                break;
            default:
                console.error(`${process.env.DB_ENGINE} is not a valid value for DB_ENGINE`)
                process.exit(1);
        }
    }

    async connect() {
        return await this._client.connect()
    }

    async getAllUsers(): Promise<User[]> {
        return await this._client.getAllUsers();
    }

    async getAllListsForUser(userId: string): Promise<IAllUserLists> {
        return await this._client.getAllLists(userId);
    }

    async getListById(userId: string, listId: string): Promise<IDBList> {
        return await this._client.getList(userId, listId);
    }
    
    async getListByName(userId: string, listId: string): Promise<IDBList> {
        return await this._client.getListByName(userId, listId);
    }
    
    async createNewList(userId: string, list: INewDBList): Promise<IDBList> {
        return await this._client.createNewList(userId, list);
    }
    
    async deleteList(userId: string, list: string): Promise<void> {
        return await this._client.deleteList(userId, list);
    }
    
    async updateList(userId: string, list: string, content: IUpdateDBListFields): Promise<IDBList> {
        return await this._client.updateList(userId, list, content);
    }
    
    async insertItemInList(user: string, list: string, item: INewDBListItem): Promise<IDBList> {
        return await this._client.insertItemToList(user, list, item);
    }
    
    async getAllItems(user: string, list: string): Promise<IDBListItem[]> {
        return await this._client.getAllItems(user, list);
    }
    
    async getItemById(user: string, list: string, item: string): Promise<IDBListItem> {
        return await this._client.getItemById(user, list, item);
    }
    
    async updateItemById(user: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList> {
        return await this._client.updateItemById(user, list, item, content);
    }
    
    async toggleCheckedOfItemById(user: string, list: string, item: string): Promise<IDBList> {
        return await this._client.toggleCheckedOfItemById(user, list, item);
    }
    
    async deleteItemById(user: string, list: string, item: string): Promise<IDBList> {
        return await this._client.deleteItemById(user, list, item);
    }
}

const engine = new DBEngine();
engine.connect()
    .catch((err) => {
        console.log(`Could not connect to ${process.env.DB_ENGINE}`);
        console.log(err);
        process.exit(1);
    })

export default engine;
