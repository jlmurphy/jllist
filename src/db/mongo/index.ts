import User from 'src/types/User';
import IDBListClient from '../../types/dbclient';
import { 
    Collection, 
    Db, 
    MongoClient, 
    MongoClientOptions, 
    ObjectId
 } from 'mongodb';
import { 
    IAllUserLists, 
    IDBList, 
    IDBListItem, 
    INewDBList, 
    INewDBListItem, 
    IUpdateDBListFields, 
    IUpdateDBListItem
 } from '../../types/List';
import { 
    checkEnvironment, 
    getUser, 
    getUserLists, 
    getUserList, 
    createNewUserList, 
    deleteUserList, 
    getItemById, 
    updateItemByID, 
    toggleCheckedOfItemById, 
    deleteItemById, 
    updateList, 
    insertNewItemToList, 
    getAllItems,
} from './handlers';

import {
    checkValuesPassedInRequest
} from '../common/handlers';

const {
    MONGO_INITDB_ROOT_USERNAME,
    MONGO_INITDB_ROOT_PASSWORD,
    MONGO_INITDB_URL,
    MONGO_INITDB_PORT,
    DB_MONGO_SUFFIX,
} = process.env;


export default class MongoDB implements IDBListClient {

    _connectOptions: MongoClientOptions = {}

    _client: MongoClient = new MongoClient('localhost:27017');
    
    /**
     * MongoDB Pointers
     */
    _users?: Db;
    _accounts?: Collection<User>;
    _lists?: Db;

    constructor() {
        this._connectOptions = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }
    }

    _connectionURL(): string {
        let _v = "";
        try {
            checkEnvironment()
            return `mongodb://${MONGO_INITDB_ROOT_USERNAME}:${MONGO_INITDB_ROOT_PASSWORD}` +
                `@${MONGO_INITDB_URL}${!MONGO_INITDB_PORT ? ':27017' : ''}`
            // return `${!MONGO_INITDB_ROOT_USERNAME ? '' : `mongodb+srv://${MONGO_INITDB_ROOT_USERNAME}:${MONGO_INITDB_ROOT_PASSWORD}@`}` + 
            // `${!MONGO_INITDB_URL ? 'localhost' : MONGO_INITDB_URL}` +
            // `${!MONGO_INITDB_PORT ? '27017' : `:${MONGO_INITDB_PORT}`}` +
            // `${DB_MONGO_SUFFIX}`
        } catch (e) {
            console.log(`MONGO VALIDATION ERRROR - ${e}`)
            throw e;
        }
    }

    async connect(): Promise<void> {
        try {
            // console.log(this._connectionURL())
            console.log(`mongodb://USERNAME:PASSWORD@${MONGO_INITDB_URL}${!MONGO_INITDB_PORT ? ':27017' : ''}`)
            this._client = new MongoClient(this._connectionURL(), this._connectOptions)
            await this._client.connect();
            console.log("Connected to Mongo server");
        } catch (e) {
            throw 'Error initializing connection'
        }
        try {
            this._users = this._client.db('users');
            this._accounts = this._users.collection('accounts');
            this._lists = this._client.db('lists');
        } catch(e) {
            throw 'Error initializing pointers'
        }
        return;
    }
    
    async getAllUsers(): Promise<User[]> {
        try {
            if (!this._accounts) throw 'No accounts collection initialized';
            //@ts-ignore
            return await this._accounts.find(
                {}, 
                { 
                    projection : { _id: 1, name: 1 } 
                }
            ).toArray();
        } catch(e){
            throw e;
        }
    }

    async getAllLists(user: string): Promise<IAllUserLists> {

        try {
            if (!this._accounts) throw 'No accounts collection initialized';
            const userLists: User = await getUser(this._accounts, user);
            if (!this._lists) throw 'No lists db initialized';
            return await getUserLists(this._lists, userLists.list_collections);
        } catch(e) {
            throw e;
        }
    }

    async getList(user: string, listId: string): Promise<IDBList> {

        try {
            return await getUserList(this, user, { _id: new ObjectId(listId) })
        } catch(e) {
            throw e;
        }
    }

    async getListByName(user: string, listName: string): Promise<IDBList> {

        try {
            await checkValuesPassedInRequest(user, listName);
            return await getUserList(this, user, { name: listName })
        } catch(e) {
            throw e;
        }
    }

    async createNewList(user: string, list: INewDBList): Promise<IDBList> {

        try {
            return await createNewUserList(this, user, list);
        } catch(e) {
            throw e;
        }
    }

    async deleteList(user: string, list: string): Promise<void> {
        try {
            await checkValuesPassedInRequest(user, list);
            return await deleteUserList(this, user, list);
        } catch(e) {
            throw e;
        }
    }

    async updateList(user: string, list: string, content: IUpdateDBListFields): Promise<IDBList> {

        try {
            await checkValuesPassedInRequest(user, list);
            return await updateList(this, user, list, content);
        } catch(e) {
            throw e;
        }
    }

    async insertItemToList(user: string, list: string, item: INewDBListItem): Promise<IDBList> {

        try {
            await checkValuesPassedInRequest(user, list);
            return await insertNewItemToList(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }

    async getAllItems(user: string, list: string): Promise<IDBListItem[]> {
        try {
            await checkValuesPassedInRequest(user, list);
            return await getAllItems(this, user, list);
        } catch(e) {
            throw e;
        }
    }

    async getItemById(user: string, list: string, item: string): Promise<IDBListItem> {
        try {
            await checkValuesPassedInRequest(user, list, item);
            return await getItemById(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }

    async updateItemById(user: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList> {

        try {
            await checkValuesPassedInRequest(user, list, item);
            return await updateItemByID(this, user, list, item, content);
        } catch(e) {
            throw e;
        }
    }

    async toggleCheckedOfItemById(user: string, list: string, item: string): Promise<IDBList> {

        try {
            await checkValuesPassedInRequest(user, list, item);
            return await toggleCheckedOfItemById(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }

    async deleteItemById(user: string, list: string, item: string): Promise<IDBList> {

        try {
            await checkValuesPassedInRequest(user, list, item);
            return await deleteItemById(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }
}
