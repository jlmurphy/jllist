import User from 'src/types/User';
import MongoDB from './index';
import { 
    Collection, 
    Db, 
    DeleteWriteOpResultObject, 
    FindAndModifyWriteOpResultObject, 
    InsertOneWriteOpResult, 
    ObjectID, 
    UpdateWriteOpResult 
} from 'mongodb';
import { 
    IAllUserLists,
    IDBList,
    IDBListItem, 
    INewDBList, 
    INewDBListItem, 
    IUpdateDBListFields, 
    IUpdateDBListItem, 
    ListQueryObject 
} from '../../types/List';

const {
    MONGO_INITDB_ROOT_USERNAME,
    MONGO_INITDB_ROOT_PASSWORD,
    MONGO_INITDB_URL,
    MONGO_INITDB_PORT,
    DB_MONGO_SUFFIX,
} = process.env;

export const checkEnvironment = () => {
    if (!!MONGO_INITDB_ROOT_USERNAME && !MONGO_INITDB_ROOT_PASSWORD) {
        throw "Mongodb - No Password Set While User Name Has Been Set";
    }
    if (!MONGO_INITDB_URL) {
        console.log("Mongodb WARNING - MONGO_INITDB_URL Not Set, Using Localhost");
    }
}

export const checkMongoCommand = (
        result: InsertOneWriteOpResult<any> 
        | DeleteWriteOpResultObject
        | UpdateWriteOpResult
    ): boolean => {

    if (!result.result) return false;
    // Check the CommandResult write ops counters
    return result.result.n === result.result.ok;
}

export const checkMongoCommandInsertResult = (result: InsertOneWriteOpResult<any>): boolean => {
    if (result.insertedCount === undefined) throw "Invalid Mongo Response";
    if (!checkMongoCommand(result)) throw `MongoCommand Results Not Properly Performed ${result}`
    return true;
}
export const checkMongoCommandDeleteResult = (result: DeleteWriteOpResultObject): boolean => {
    if (result.result === undefined) throw "Invalid Mongo Delete Response";
    if (!checkMongoCommand(result)) throw `MongoCommand Results Not Properly Performed ${result}`
    return true;
}

export const checkMongoCommandUpdateResult = (result: FindAndModifyWriteOpResultObject<any>): boolean => {
    if (result.ok === undefined) throw "Invalid Mongo Update Response";
    return result.ok === 1;
}


export const getUser = async (accountCollection: Collection, user: string): Promise<User> => {
    return await accountCollection.findOne({ name: user });
}

export const getUserCollection = async (engineClient: MongoDB, user: string): Promise<Collection> => {
    try {
        if (!engineClient._accounts) throw 'No accounts collection initialized';
        const userLists: User = await getUser(engineClient._accounts, user);
        if (!engineClient._lists) throw 'No lists db initialized';
        return engineClient._lists.collection(userLists.list_collections);
    } catch(e) {
        throw e;
    }
}

export const getUserLists = async (listsDB: Db, listCollId: string): Promise<IAllUserLists> => {
    return await listsDB.collection(listCollId)
        .find()
        .toArray();
}

export const getUserList = async (engine: MongoDB, userId: string, listQueryObject: ListQueryObject): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);
        const result = await coll.findOne(listQueryObject);
        if (!result) throw `No list exists for user with id ${userId}`;
        return result;
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const createNewUserList = async (engine: MongoDB, userId: string, list: INewDBList): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);
        const result = await coll.insertOne(list);
        if (!result) throw `No list exists for user with id ${userId}`;
        checkMongoCommandInsertResult(result);
        if (result.insertedCount !== 1) throw `Wrong number of docs inserted - ${result.insertedCount}`;
        return result.ops[0];
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const deleteUserList = async (engine: MongoDB, userId: string, list: string): Promise<void> => {
    try {
        const coll = await getUserCollection(engine, userId);
        const result = await coll.deleteOne( { _id: new ObjectID(list) });
        if (!result) throw `No list exists for user with id ${userId}`;
        checkMongoCommandDeleteResult(result)
        return
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const updateList = async (engine: MongoDB, userId: string, list: string, content: IUpdateDBListFields): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);
        const result = await coll.findOneAndUpdate( 
            { _id: new ObjectID(list) },
            {
                "$set": content
            },
            { returnDocument: 'after' }
        );
        if (!result) throw `No list exists for user with id ${userId}`;
        if (!checkMongoCommandUpdateResult(result)) throw "Invalid write operation"
        return result.value
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const insertNewItemToList = async (engine: MongoDB, userId: string, list: string, item: INewDBListItem): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);

        const newItem = { 
            ...item,
            _id: new ObjectID(),
        };
        // We make sure the item is unchecked at first
        newItem.checked = newItem.checked === undefined ? 0 : newItem.checked;

        const result = await coll.findOneAndUpdate( 
            { _id: new ObjectID(list) },
            {
                "$push": { items: newItem }
            },
            { returnDocument: 'after' }
        );
        if (!result) throw `Invalid list insert for ${userId} on collection id ${list}`;
        if (!checkMongoCommandUpdateResult(result)) throw "Invalid write operation"
        return result.value
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const getAllItems = async (engine: MongoDB, userId: string, list: string): Promise<IDBListItem[]> => {
    try {
        const coll = await getUserCollection(engine, userId);
        let result = await coll.find( 
            { _id: new ObjectID(list) },
            //@ts-ignore
            { projection: { _id: 0, items: 1 }},
        ).toArray();
        if (result.length !== 1) throw `Could not find specific single list document ${list} for ${userId}`;
        result = result[0]
        //@ts-ignore
        return result.items;
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const getItemById = async (engine: MongoDB, userId: string, list: string, item: string): Promise<IDBListItem> => {
    try {
        const coll = await getUserCollection(engine, userId);
        let result = await coll.find( 
            { 
                _id: new ObjectID(list),
                'items._id': new ObjectID(item)
            },
            { projection: { 'items.$': true }},
        ).toArray();
        if (result.length !== 1) throw `Could not find specific single list document ${list} for ${userId}`;
        //@ts-ignore
        if (result[0].items.length !== 1) throw `Could not find specific single list item ${item} for list ${list} for user ${userId}`;
        result = result[0]
        //@ts-ignore
        return result.items[0];
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const doesItemExistById = async (coll: Collection, list: string, item: string): Promise<void> => {
    try {
        let pev = await coll.find( 
            { 
                _id: new ObjectID(list),
                'items._id': new ObjectID(item)
            },
            { projection: { 'items.$': true }},
        ).toArray();
        if (pev.length === 0) throw `List item ${item} not found in list ${list}`;
        return;
    } catch(e) {
        throw e;
    }
}

export const updateItemByID = async (engine: MongoDB, userId: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);
        await doesItemExistById(coll, list, item);
        const updateQuery = {};
        for (let key of Object.keys(content)) {
            //@ts-ignore
            updateQuery[`items.$.${key}`] = content[key];
        }
        let result = await coll.findOneAndUpdate( 
            { 
                _id: new ObjectID(list),
                'items._id': new ObjectID(item),
            },
            { 
                '$set': updateQuery//{ 'items' : { _id: new ObjectID(item) } }
            },
            { returnDocument: 'after' }
        );
        if (!checkMongoCommandUpdateResult(result)) throw "Invalid updateItemByID write operation"
        return result.value;
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const toggleCheckedOfItemById = async (engine: MongoDB, userId: string, list: string, item: string): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);
        await doesItemExistById(coll, list, item);
        let result = await coll.findOneAndUpdate( 
            { 
                _id: new ObjectID(list),
                'items._id': new ObjectID(item),
            },
            { 
                '$inc': {
                    'items.$.done': 1
                }
            },
            { returnDocument: 'after' }
        );
        if (!checkMongoCommandUpdateResult(result)) throw "Invalid toggleCheckedOfItemById write operation"
        return result.value;
    } catch(e) {
        console.log(e);
        throw e;
    }
}

export const deleteItemById = async (engine: MongoDB, userId: string, list: string, item: string): Promise<IDBList> => {
    try {
        const coll = await getUserCollection(engine, userId);
        await doesItemExistById(coll, list, item);
        let result = await coll.findOneAndUpdate( 
            { 
                _id: new ObjectID(list)
            },
            { 
                '$pull': { 'items' : { _id: new ObjectID(item) } }
            },
            { returnDocument: 'after' }
        );
        if (!checkMongoCommandUpdateResult(result)) throw "Invalid deleteItemById write operation"
        return result.value;
    } catch(e) {
        console.log(e);
        throw e;
    }
}