import { IUpdateDBListItem } from "src/types/List";

/**
 *  {string}
 */
export const getAllUsersQuery = `SELECT _id, name FROM users`;

export const returnListKeys = "RETURNING _id, name, description, items"

/**
 * 
 * @param user 
 * @returns  {string}
 */
export const getFullUserObjectQuery = (user: string) => `SELECT _id, name, list_collections 
FROM users WHERE _id='${user}'`

/**
 * 
 * @param user 
 * @returns  {string}
 */
export const getFullUserObjectByNameQuery = (user: string) => `SELECT _id, name, list_collections 
FROM users WHERE name='${user}'`

/**
 * 
 * @param list_collections_id 
 * @returns  {string}
 */
export const getAllUserListsQuery = (list_collections_id: string) => {
    return `SELECT _id, name, description, items FROM _${list_collections_id}`
}

/**
 * 
 * @param list_collections_id 
 * @param listId 
 * @returns  {string}
 */
export const getUserListQuery = (list_collections_id: string, listId: string, field?: string) => {
    return `
    SELECT _id, name, description, items 
    FROM _${list_collections_id} 
    WHERE ${!!field ? field : '_id'} ='${listId}'`;
}

/**
 * 
 * @param list_collections_id 
 * @param listId 
 * @returns  {string}
 */
export const getUserListItemsQuery = (list_collections_id: string, listId: string): string => {
    return `SELECT items FROM _${list_collections_id} WHERE _id='${listId}'`;
}

/**
 * Returns the query string to get the array position of the json array object
 * of the item we're looking for.
 * 
 * @param list_collections_id 
 * @param listId 
 * @param itemId 
 * @returns 
 */
export const getUserItemListPositionIndexQueryString = (
        list_collections_id: string, 
        listId: string,
        itemId: string
    ):string => {
        return `Cast((select arr.position  FROM _${list_collections_id}, jsonb_array_elements(items) with ordinality arr(item, position) 
        WHERE _id = '${listId}' and arr.item->>'_id' = '${itemId}') as int)`
}

/**
 * Returns the query to fetch the item we're looking for
 * 
 * @param list_collections_id 
 * @param listId 
 * @param itemId 
 * @returns {string}
 */
export const getUserListItemByIdQuery = (
    list_collections_id: string, 
    listId: string,
    itemId: string,
): string => {
    return `
        SELECT arr.item
        FROM _${list_collections_id},jsonb_array_elements(items) with ordinality arr(item, position) 
        WHERE _id = '${listId}' and arr.position=${getUserItemListPositionIndexQueryString(list_collections_id,listId, itemId)}
    `
}

/**
 * Returns query to insert a new list into the user's lists table
 * 
 * @param list_collections_id 
 * @param keys 
 * @param content 
 * @returns  {string}
 */
export const insertUserListQuery = (list_collections_id: string, keys: string, content: string) => {
    let q = `INSERT INTO _${list_collections_id} ${keys} ${content}`;
    q += `RETURNING ${keys.replace('(', '').replace(')', '')}`;
    return q;
}

/**
 * Returns query to delete a list for a user
 * 
 * @param list_collections_id the name of the table
 * @param listId the id of the list
 * @param conditions optional SQL conditions that cand be tacked onto _id={listId}
 * @returns {string}
 */
export const deleteUserListQuery = (list_collections_id: string, listId: string, conditions?: string) => {
    return `DELETE FROM _${list_collections_id} WHERE _id='${listId}'${!!conditions ? `, ${conditions}` : ''};`
}

/**
 * 
 * @param list_collections_id the name of the table
 * @param listId the id of the list
 * @param setters the setting values for a new row , ie: SET column1 = value1, column2 = value2, ...
 * @returns {string}
 */
export const updateUserListQuery = (list_collections_id: string, listId: string, setters: string) => {
    return `UPDATE _${list_collections_id} SET ${setters} WHERE _id='${listId}' ${returnListKeys}`
}

/**
 * Returns the update query to update a single item within a list
 * 
 * @param list_collections_id 
 * @param listId 
 * @param itemId 
 * @param paths 
 * @param sets 
 * @returns {string} SQL Query string
 */
export const updateUserListItemByIdQuery = (
        list_collections_id: string, 
        listId: string,
        itemId: string,
        paths: string,
        sets: string
    ): string => {
        // console.log(paths)
        // console.log(sets)
    return `with item_update as (
        select ${paths}
                from _${list_collections_id}
                , jsonb_array_elements(items) with ordinality arr(item, index)
            where item->>'_id' = '${itemId}'
              and _id = '${listId}'
    )
    update _${list_collections_id}
        set items = ${sets}
        from item_update
        where _id = '${listId}'
        ${returnListKeys}
    `
}

/**
 * Returns the query that updates the list item's done flag
 * 
 * @param list_collections_id 
 * @param listId 
 * @param itemId
 * @returns 
 */
export const updateUserListItemDoneValueQuery = (
        list_collections_id: string, 
        listId: string,
        itemId: string,
    ): string => {
    return `
    with item_index as (
        select ('{'||index-1||',done}')::text[] as path_done,
               (item)::json->>'done' as path_done_value
                from _${list_collections_id}
                , jsonb_array_elements(items) with ordinality arr(item, index)
            where item->>'_id' = '${itemId}'
              and _id = '${listId}'
    )
    update _${list_collections_id}
    -- This updates the counter by 1
    set items = jsonb_set(items, item_index.path_done, to_jsonb((item_index.path_done_value::int + 1)), false)
    from item_index
    where _id = '${listId}'
    ${returnListKeys};
    `
}

/**
 * 
 * @param list_collections_id 
 * @param listId 
 * @param itemId 
 * @returns 
 */
export const deleteUserListItemById = (
    list_collections_id: string, 
    listId: string,
    itemId: string,
):string => {
    return `
    UPDATE _${list_collections_id} SET items = items - 
        Cast((SELECT position - 1 FROM _${list_collections_id}, jsonb_array_elements(items) with ordinality arr(item, position) 
        WHERE _id = '${listId}' and item->>'_id' = '${itemId}') as int)
        WHERE _id = '${listId}'
        ${returnListKeys}
    `
}
