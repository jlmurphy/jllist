import { Pool } from 'pg';
import IListDBClient from 'src/types/dbclient';
import User from 'src/types/User';
import { 
    IAllUserLists, 
    IDBList, 
    IDBListItem, 
    INewDBList, 
    INewDBListItem, 
    IUpdateDBListFields, 
    IUpdateDBListItem 
} from 'src/types/List';
import { 
    getUserList, 
    getAllItems, 
    insertNewItemToList, 
    createNewUserList,
    getUser, 
    getItemById, 
    updateItemByID, 
    toggleCheckedOfItemById, 
    deleteItemById, 
    checkEnvironmentForValues,
    deleteUserList, 
    getAllUsers, 
    getUserLists, 
    updateList,
    getUserListByName 
} from './handlers';
import { checkMainValuesPassedInRequestWithOpenThird, checkUserListPassedInRequest, checkUserPassedInRequest, checkValuesPassedInRequest } from '../common/handlers';

export default class PostgreSQL implements IListDBClient {

    _client: Pool
    
    constructor() {
        this._client = new Pool();
    }
    
    async connect(): Promise<void> {
        try {
            await checkEnvironmentForValues();
        } catch(e) {
            console.error(e);
            process.exit(1);
        }
        return;
    }
    
    async getAllUsers(): Promise<User[]> {
        try {
            return await getAllUsers(this)
        } catch(e) {
            throw e;
        }
    }

    async getAllLists(user: string): Promise<IAllUserLists> {
        try {
            checkUserPassedInRequest(user);
            return getUserLists(this, user)
        } catch(e) {
            throw e;
        }
    }

    async getList(user: string, listId: string): Promise<IDBList> {
        try {
            checkUserListPassedInRequest(user, listId);
            return await getUserList(this, user, listId)
        } catch(e) {
            throw e;
        }
    }

    async getListByName(user: string, listName: string): Promise<IDBList> {
        try {
            checkUserListPassedInRequest(user, listName);
            return await getUserListByName(this, user, listName)
        } catch(e) {
            throw e;
        }
    }

    async createNewList(user: string, list: INewDBList): Promise<IDBList> {
        try {
            checkMainValuesPassedInRequestWithOpenThird(user, "list", list)
            return await createNewUserList(this, user, list);
        } catch(e) {
            throw e;
        }
    }

    async deleteList(user: string, list: string): Promise<void> {
        try {
            checkUserListPassedInRequest(user, list);
            return await deleteUserList(this, user, list)
        } catch(e) {
            throw e;
        }
    }

    async updateList(user: string, list: string, content: IUpdateDBListFields): Promise<IDBList> {
        try {
            if (content.items !== undefined) throw "Items cannot be added through this method";
            //@ts-ignore
            checkMainValuesPassedInRequestWithOpenThird(user, list)
            return await updateList(this, user, list, content);
        } catch(e) {
            throw e;
        }
    }

    async insertItemToList(user: string, list: string, item: INewDBListItem): Promise<IDBList> {
        try {
            checkMainValuesPassedInRequestWithOpenThird(user, list, item)
            return await insertNewItemToList(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }

    async getAllItems(user: string, list: string): Promise<IDBListItem[]> {
        try {
            checkUserListPassedInRequest(user, list);
            return await getAllItems(this, user, list);
        } catch(e) {
            throw e;
        }
    }

    async getItemById(user: string, list: string, item: string): Promise<IDBListItem> {
        try {
            checkMainValuesPassedInRequestWithOpenThird(user, list, item)
            return await getItemById(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }

    async updateItemById(user: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList> {
        try {
            checkMainValuesPassedInRequestWithOpenThird(user, list, item)
            return await updateItemByID(this, user, list, item, content);
        } catch(e) {
            throw e;
        }
    }

    async toggleCheckedOfItemById(user: string, list: string, item: string): Promise<IDBList> {
        try {
            checkMainValuesPassedInRequestWithOpenThird(user, list, item)
            return toggleCheckedOfItemById(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }

    async deleteItemById(user: string, list: string, item: string): Promise<IDBList> {
        try {
            checkMainValuesPassedInRequestWithOpenThird(user, list, item)
            return await deleteItemById(this, user, list, item);
        } catch(e) {
            throw e;
        }
    }
}