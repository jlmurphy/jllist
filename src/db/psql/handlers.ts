import { ObjectId } from "mongodb";
import User from "src/types/User";
import PostgreSQL from "./index";
import { 
    IAllUserLists,
    IDBList, 
    IDBListItem, 
    INewDBList,
    INewDBListItem,
    IUpdateDBListFields, 
    IUpdateDBListItem
} from "src/types/List";
import { 
    getFullUserObjectByNameQuery, 
    getUserListQuery, 
    insertUserListQuery, 
    updateUserListQuery, 
    updateUserListItemByIdQuery, 
    getUserListItemsQuery, 
    getUserListItemByIdQuery, 
    updateUserListItemDoneValueQuery, 
    deleteUserListItemById, 
    deleteUserListQuery,
    getAllUserListsQuery,
    getAllUsersQuery
} from "./queries";
import { checkUserPassedInRequest, checkValuesPassedInRequest } from "../common/handlers";

export interface SQLSetterOptions {
    arrays?: Set<string>,
    jsonArrays?: Set<string>,
}

export const checkEnvironmentForValues = async () => {
    const {
        PGUSER,
        PGHOST,
        PGPASSWORD,
        PGDATABASE,
        PGPORT
    } = process.env;
    if (!PGUSER) throw "PGUSER not set";
    if (!PGPASSWORD) throw "PGPASSWORD not set";
    if (!PGHOST) throw "PGHOST not set";
    if (!PGDATABASE) throw "PGDATABASE not set";
    if (!PGPORT) throw "PGPORT not set";
}

export const listOfJSONArrayKeys = new Set(['items']);

export const buildJSONBArraySetter = (key: string, value: string): string => {
    return `${key} = ${key} || '${value.replace(`'`, `''`)}' ::jsonb`;
}

export const buildSettersFrom = async (content: any, options?: SQLSetterOptions): Promise<string> => {
    try {
        let result = '';
        let keys = Object.keys(content);
        let hasOptions = options !== undefined;
        let hasJSONArrays = options?.jsonArrays !== undefined;
        let jsonArrays = new Set();
        //@ts-ignore
        if (hasJSONArrays) jsonArrays = options?.jsonArrays;
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i], value = content[key];
            if (0 < i) result += ', '
            if (typeof value === 'object') {
                value = JSON.stringify(JSON.parse(JSON.stringify(value)))
            }
            if (hasOptions) {
                if (hasJSONArrays && jsonArrays.has(key)) {
                    result += buildJSONBArraySetter(key, value);
                    continue;
                }
            }
            result += `${key} = '${value.replace(`'`, `''`)}'`;
        }
        return result
    } catch(e) {
        throw e;
    }
}


export interface IUpdateItemPathsAndSets {
    paths: string,
    sets: string
}

/**
 * 
 * @param content List Item update item
 * @returns 
 */
export const buildItemSettersFrom = async (content: IUpdateDBListItem): Promise<IUpdateItemPathsAndSets> => {
    let paths = '';
    let sets = '';

    let keys = Object.keys(content);

    for (let i = 0; i < keys.length; i++) {
        //@ts-ignore
        let key = keys[i];
        if (0 < i) paths += ',\n'
        paths += `('{'||index-1||',${key}}')::text[] as path_${key}`;
    }

    for (let i = keys.length - 1; 0 <= i; i--) {
        //@ts-ignore
        let key = keys[i], value = content[key];
        switch (typeof value) {
            case 'string':
                value = `"${value}"`;
                break;
            default: break;
        }
        if (i === keys.length - 1) {
            sets = `jsonb_set(items, item_update.path_${key}, '${value}', false)`
        } else {
            // /jsonb_set(items, item_update.path_name, '"This is an new update2"', false)
            sets = `jsonb_set(${sets}, item_update.path_${key}, '${value}', false)`
        }
    }

    return { paths, sets }
}

export const getAllUsers = async (pgsql: PostgreSQL): Promise<User[]> => {
    try {
        const result = await pgsql._client.query(getAllUsersQuery);
        if (result.rows.length < 1) throw "No user found"
        return result.rows;
    } catch(e) {
        throw e;
    }
}

/**
 * Returns a user profile
 * 
 * @param pgsql PostgreSQL client class
 * @param user username (or user's name)
 * @returns {User}
 */
 export const getUser = async (pgsql: PostgreSQL,  user: string): Promise<User> => {
    try {
        const result = await pgsql._client.query(getFullUserObjectByNameQuery(user));
        if (result.rows.length !== 1) throw "No user found";
        return result.rows[0];
    } catch(e) {
        throw e;
    }
}

/**
 * 
 * @param pgsql 
 * @param user 
 * @returns 
 */
export const getUserLists = async (pgsql: PostgreSQL, user: string): Promise<IAllUserLists> => {
    try {
        if (!user) throw 'No userId provided';
        const result = await pgsql._client.query(getFullUserObjectByNameQuery(user));
        if (result.rows.length !== 1) throw "No user found"
        const lists = await pgsql._client.query(getAllUserListsQuery(result.rows[0].list_collections))
        return lists.rows;
    } catch(e) {
        throw e;
    }
}

/**
 * 
 * @param pgsql 
 * @param user 
 * @param listId 
 * @returns 
 */
export const getUserList = async (pgsql: PostgreSQL, user: string, listId: string): Promise<IDBList> => {
    try {
        await checkValuesPassedInRequest(user, listId)
        const result = await pgsql._client.query(
            getFullUserObjectByNameQuery(user)
        );
        if (result.rows.length !== 1) throw "No user found"
        const lists = await pgsql._client.query(
            getUserListQuery(
                result.rows[0].list_collections, 
                listId
            )
        );
        if (lists.rows.length !== 1) throw `No list found with id ${listId}`;
        return lists.rows[0];
    } catch(e) {
        throw e;
    }
}

export const getUserListByName = async (pgsql: PostgreSQL, user: string, name: string): Promise<IDBList> => {
    try {
        if (!user) throw 'No userId provided';
        const result = await pgsql._client.query(
            getFullUserObjectByNameQuery(user)
        );
        if (result.rows.length !== 1) throw "No user found"
        const lists = await pgsql._client.query(
            getUserListQuery(
                result.rows[0].list_collections, 
                name, 
                'name'
            )
        );
        if (lists.rows.length !== 1) throw `No list found with name ${name}`;
        return lists.rows[0];
    } catch(e) {
        throw e;
    }
}

export const createNewUserList = async (pgsql: PostgreSQL, user: string, list: INewDBList): Promise<IDBList> => {

    try {
        
        checkUserPassedInRequest(user);
        if (!!list.name && list.name.length > 96) throw 'List name is too long'
        list._id = `${(new ObjectId()).toString()}`;
        /**
         * Get user
         */
        const result = await getUser(pgsql, user)

        /**
         * Build the Keys and Values substrings of the query string
         */

        let keys = "(";
        let values = "VALUES(";
        const ks = Object.keys(list);
        for (let i = 0; i < ks.length; i ++) {
            //@ts-ignore
            let key = ks[i];
            //@ts-ignore
            if (i === 0) {
                //@ts-ignore
                values += `'${list[key]}'`;
                keys += `${key}`;
            } else {
                //@ts-ignore
                if (key === 'items') {
                    /**
                     * We need to inject a JSON string (not an object), so we remove all whitespace and escapes
                     */
                    let toAdd = await JSON.stringify(JSON.parse(JSON.stringify(list.items)));
                    /**
                     * Handle apostrophes by swapping them with double quotes
                     */
                    toAdd = await toAdd.replace(`'`, `''`)
                    values += `, '${toAdd}'`;
                } else {
                    //@ts-ignore
                    values += `, '${list[key]}'`;
                }
                keys += `, ${key}`;
            }
        }

        /**
         * Close off the string
         */
        keys += ')';
        values += ')';

        const lists = await pgsql._client.query(insertUserListQuery(result.list_collections, keys, `${values}`))
        return lists.rows[0];
    } catch(e) {
        throw e;
    }
}


export const deleteUserList = async (pgsql: PostgreSQL, user: string, list: string): Promise<void> => {
    try {
        const result = await getUser(pgsql, user);
        await pgsql._client.query(deleteUserListQuery(result.list_collections, list));
        return;
    } catch(e) {
        throw e;
    }
}

export const updateList = async (pgsql: PostgreSQL, user: string, list: string, content: IUpdateDBListFields): Promise<IDBList>  => {
    try {
        if (!!content.name && content.name.length > 96) throw 'List name is too long'
        const result = await getUser(pgsql, user);
        let setters = await buildSettersFrom(content);
        const update = await pgsql._client.query(updateUserListQuery(result.list_collections, list, setters));
        return update.rows[0];
    } catch(e) {
        throw e;
    }
}

export const insertNewItemToList = async (pgsql: PostgreSQL, user: string, list: string, item: INewDBListItem): Promise<IDBList> => {
    
    try { 
        const result = await getUser(pgsql, user);
        let setters = await buildSettersFrom(
            {
                items: [{ 
                    ...item, 
                    done: 1, 
                    _id: (new ObjectId()).toString()
                }], 
            }, 
            { 
                jsonArrays: listOfJSONArrayKeys 
            }
        );
        const q = updateUserListQuery(result.list_collections, list, setters);
        const update = await pgsql._client.query(q);
        return update.rows[0];
    } catch(e) {
        throw e;
    }
}


export const updateItemByID = async (pgsql: PostgreSQL, user: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList> => {
    try { 
        const result = await getUser(pgsql, user);
        const { paths, sets } = await buildItemSettersFrom(content)
        const update = await pgsql._client.query(
            updateUserListItemByIdQuery(result.list_collections, list, item, paths, sets)
        );
        return update.rows[0];
    } catch(e) {
        throw e;
    }
}

export const getAllItems = async (pgsql: PostgreSQL, user: string, list: string): Promise<IDBListItem[]> => {
    try { 
        const result = await getUser(pgsql, user);
        const quer = getUserListItemsQuery(result.list_collections, list);
        const get = await pgsql._client.query(quer);
        return get.rows[0]?.items;
    } catch(e) {
        throw e;
    }
}

export const getItemById = async (pgsql: PostgreSQL, user: string, list: string, item: string): Promise<IDBListItem> => {
    try { 
        const result = await getUser(pgsql, user);
        const quer = getUserListItemByIdQuery(result.list_collections, list, item);
        const get = await pgsql._client.query(quer);
        if (get.rows.length !== 1) throw `No such item with id ${item} in list with id ${list} - 1`
        if (get.rows[0].item === undefined) throw `No such item with id ${item} in list with id ${list} - 2`
        return get.rows[0].item;
    } catch(e) {
        throw e;
    }
}

export const toggleCheckedOfItemById = async (pgsql: PostgreSQL, user: string, list: string, item: string): Promise<IDBList> => {
    try { 
        const result = await getUser(pgsql, user);
        const quer = updateUserListItemDoneValueQuery(result.list_collections, list, item);
        const get = await pgsql._client.query(quer);
        if (get.rows.length !== 1) throw `No such list with id ${list}`
        return get.rows[0];
    } catch(e) {
        throw e;
    }
}

export const deleteItemById = async (pgsql: PostgreSQL, user: string, list: string, item: string): Promise<IDBList> => {
    try { 
        const result = await getUser(pgsql, user);
        const quer = deleteUserListItemById(result.list_collections, list, item);
        const get = await pgsql._client.query(quer);
        if (get.rows.length !== 1) throw `No such list with id ${list}`
        return get.rows[0];
    } catch(e) {
        throw e;
    }
}
