import { 
    Request, 
    Response 
} from 'express';
import { 
    StatusCodes, 
    ReasonPhrases, 
} from 'http-status-codes';

export const badRequestResponse = (
        res: Response,
        e?: any, 
    ): Response<any, Record<string, any>> => {

    res.status(StatusCodes.BAD_REQUEST)
    // If the error returned is a string, add it to the message
    return res.send(ReasonPhrases.BAD_REQUEST + `${typeof e === 'string' ? ` - ${e}` : ''}`)
}

export const serverErrorResponse = (
        res: Response,
        e?: any, 
        flag?: any
    ): Response<any, Record<string, any>> => {
    if (typeof e === 'string' && e.indexOf('No') === 0) return badRequestResponse(res, e);
    res.status(StatusCodes.INTERNAL_SERVER_ERROR)
    // If the error returned is a string, add it to the message
    let error = !!e ?`${typeof e === 'string' ? `${e}` : ''}` :  ReasonPhrases.INTERNAL_SERVER_ERROR;
    if (!!flag) error += ` - ${flag}`
    return res.send(error)
}

export const notFoundResponse = (
        res: Response,
    ): Response<any, Record<string, any>> => {

    res.status(StatusCodes.NOT_FOUND)
    // If the error returned is a string, add it to the message
    return res.send(ReasonPhrases.NOT_FOUND)
}

export const unauthorizedResponse = (
        res: Response,
    ): Response<any, Record<string, any>> => {

    res.status(StatusCodes.UNAUTHORIZED)
    // If the error returned is a string, add it to the message
    return res.send(ReasonPhrases.UNAUTHORIZED)
}

export const okResponse = (
        req: Request,
        res: Response,
        body?: any, 
    ): Response<any, Record<string, any>> => {
    
    res.status(StatusCodes.OK)
    switch (req.headers['content-type']) {
        case 'application/json':
            return res.json({
                code: StatusCodes.OK,
                message: ReasonPhrases.OK,
                data: body,
            })
        default:
            return res.send("Ok")
    }
}