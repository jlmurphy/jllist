import engine from '../../db';

import { 
    Request, 
    Response 
} from 'express';

import { 
    badRequestResponse, 
    okResponse,
    serverErrorResponse 
} from '../../utils/http-methods';

import { 
    IDBList, 
    IUpdateDBListItem 
} from 'src/types/List';

import { 
    ObjectID 
} from 'mongodb';

// import { assert } from 'chai'


/// Convenience Methods 

export class ObjectSizeBounds{
    min: number = 0
    max: number = 0
    constructor(min?: number, max?: number) {
        this.min = !!min ? min : 0;
        this.max = !!max ? max : 0;
    }
}

const checkBodyForUpdate = async (body: any, maxKeys?: ObjectSizeBounds) => {
    if (body === undefined) throw 'Body is undefined'
    const keys = await Object.keys(body)
    const keyCount = keys.length;
    if (keyCount < 0) throw 'Empty body'
    if (
            maxKeys !== undefined 
            && !(
                    maxKeys.min <= keyCount && keyCount <= maxKeys.max
                )
        ) throw `Body contains an invalid number of keys {${maxKeys.min},${maxKeys.max}}`;
    return;
}

const checkListPrereqs = (userId: string, listid: string) => {
    if (!userId) throw "No userId provided"
    if (!listid) throw "No listid provided"
}

const checkItemPrereqs = (userId: string, listid: string, itemid: string) => {
    try {
        checkListPrereqs(userId, listid);
        if (!itemid) throw "No itemid provided"
    } catch(e) {
        throw e;
    }
}


const itemUpdateSafeguards = async (body: IUpdateDBListItem) => {
    if (body === undefined) throw 'Empty Body'
    const { _id, name, description, checked} = body;
    await checkBodyForUpdate(body, new ObjectSizeBounds(1, 3))
    if (!!_id) throw "Item _id cannot be modified"
    if (!!description && description.length > 256) throw "Decription is too long";
}


/// METHODS

export const getAllLists = async (req: Request, res: Response) => {
    const {
        userId
    } = req.headers;
    let result;
    try {
        if (!userId) throw "No UserId provided in headers";
        result = await engine.getAllListsForUser(String(userId))
    } catch(e) {
        return serverErrorResponse(res, e, '101')
    }
    return okResponse(req, res, result)
}

export const getListByRouteId = async (req: Request, res: Response) => {
    const {
        listid,
    } = req.params;
    const {
        userId
    } = req.headers;

    try {
        let result;
        result = await engine.getListById(String(userId), listid)
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e);
        return serverErrorResponse(res, e, '101')
    }
}

/**
 * Make sure the search checks
 * @param req 
 * @param res 
 * @returns 
 */
export const getListByQueryName = async (req: Request, res: Response) => {
    const {
        name
    } = req.query;
    const {
        userId,
    } = req.headers;

    
    try {
        if (!name) throw "No name provided"
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);
    }

    try {
        let result = await engine.getListByName(String(userId), String(name));
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e);
        return serverErrorResponse(res, e, 102)
    }
}

export const createNewList = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        name,
        description,
        items
    } = req.body;
    const {
        userId,
    } = req.headers;

    // Checks if there's a name
    try {
        if (!name) throw "No name provided"
        //Check the length of the name
        if (name.length > 96) throw "Name is too long, max length is 96 characters";
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }
    // Checks if there's a name
    try {
        // Add ids to the items if they exist
        if (!!items) {
            if (!Array.isArray(items)) throw "Key items is not an array";
            try {
                for (let i = 0; i < req.body.items.length; i++) {
                    req.body.items[i]._id = new ObjectID();
                }
            } catch(e) {
                throw e;
            }
        } else {
            items = []
        }
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Create the list

    try {
        const result = await engine.createNewList(String(userId), req.body);
        return okResponse(req, res, {
            created: result
        });
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102)
    }
}

export const deleteList = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid
    } = req.params;
    const {
        userId,
    } = req.headers;

    // Checks prereqs of the list query keys
    try {
        checkListPrereqs(String(userId), listid);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Delete the list

    try {
        await engine.deleteList(String(userId), listid);
        return okResponse(req, res);
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102)
    }
}

export const updateList = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid
    } = req.params;
    const {
        userId,
    } = req.headers;
    const {
        name,
        description,
        items,
    } = req.body;

    // Checks if there's a name
    try {
        //Check the length of the name
        if (!!name && name.length > 96) throw "Name is too long, max length is 96 characters";
        checkListPrereqs(String(userId), listid);
        if (req.body === undefined || Object.keys(req.body).length === 0) throw 'Empty Body'
        await checkBodyForUpdate(req.body, new ObjectSizeBounds(1, 2))
        if (!!items) throw 'Cannot update all items via this method, use POST /list/:listid/item instead';
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Delete the list

    try {
        const result = await engine.updateList(String(userId), listid, req.body);
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102)
    }
}


// Items

export const insertNewItemToList = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid
    } = req.params;
    const {
        userId,
    } = req.headers;

    // Checks if there's a name
    try {
        checkListPrereqs(String(userId), listid);
        await itemUpdateSafeguards(req.body);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Insert the new item   

    try {
        const result = await engine.insertItemInList(String(userId), listid, req.body);
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102)
    }
}


export const getAllItems = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid
    } = req.params;
    const {
        userId,
    } = req.headers;

    // Checks if there's a name
    try {
        checkListPrereqs(String(userId), listid);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Delete the list
    
    try {
        const result = await engine.getAllItems(String(userId), listid);
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102)
    }
}

export const getSingleItemById = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid,
        itemid
    } = req.params;
    const {
        userId,
    } = req.headers;

    // Checks if there's a name
    try {
        checkItemPrereqs(String(userId), listid, itemid);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Delete the list
    
    try {
        const result = await engine.getItemById(String(userId), listid, itemid);
        return okResponse(req, res, result);
    } catch(e) {
        return serverErrorResponse(res, null, 102);
    }
}

const updateItemByIdAsIs = async (user: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList> => {
    return await engine.updateItemById(user, list, item, content);
}

const updateItemByIdAsChecked = async (user: string, list: string, item: string, checked: number): Promise<IDBList> => {
    if (checked === undefined) throw "No value passed for checked";
    return await engine.updateItemById(user, list, item, { checked });
}

const updateItemByIdName = async (user: string, list: string, item: string, name: string): Promise<IDBList> => {
    if (name === undefined) throw "No value passed for checked";
    return await engine.updateItemById(user, list, item, { name });
}

const updateItemByIdDescription = async (user: string, list: string, item: string, description: string): Promise<IDBList> => {
    if (description === undefined) throw "No value passed for checked";
    return await engine.updateItemById(user, list, item, { description });
}

export const updateSingleItemById = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid,
        itemid
    } = req.params;
    const {
        userId,
    } = req.headers;
    const {
        _id,
        description
    } = req.body;

    // Checks if there's a name
    try {
        checkItemPrereqs(String(userId), listid, itemid);
        await itemUpdateSafeguards(req.body);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Insert the new item   

    try {
        const result = await updateItemByIdAsIs(String(userId), listid, itemid, req.body);
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102);
    }
}

export const toggleCheckedForeSingleItemById = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid,
        itemid
    } = req.params;
    const {
        userId,
    } = req.headers;

    // Checks if there's a name
    try {
        checkItemPrereqs(String(userId), listid, itemid);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Insert the new item   

    try {
        const result = await engine.toggleCheckedOfItemById(String(userId), listid, itemid);
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e)
        return serverErrorResponse(res, e, 102)
    }
}

export const deleteSingleItemById = async (req: Request, res: Response): Promise<Response<any, Record<string, any>>> => {
    let {
        listid,
        itemid
    } = req.params;
    const {
        userId,
    } = req.headers;

    // Checks if there's a name
    try {
        checkItemPrereqs(String(userId), listid, itemid);
    } catch(e) {
        console.log(e)
        return badRequestResponse(res, e);    
    }

    // Delete the list
    
    try {
        const result = await engine.deleteItemById(String(userId), listid, itemid);
        return okResponse(req, res, result);
    } catch(e) {
        console.log(e)
        if (e === 'List item not found') return badRequestResponse(res, e);   
        return serverErrorResponse(res, e, 102)
    }
}