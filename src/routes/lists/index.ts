import itemNode from './items';
import { 
    Router, 
    NextFunction, 
    Request, 
    Response 
} from 'express';
import { 
    createNewList, 
    deleteList, 
    getAllLists, 
    getListByQueryName, 
    getListByRouteId, 
    updateList 
} from './handlers';
import { 
    unauthorizedResponse 
} from '../../../src/utils/http-methods';

const listNode = Router({ mergeParams: true });

listNode.use((req: Request, res: Response, next: NextFunction) => {
    const {
        userId
    } = req.headers;
    try {
        if (!userId) throw 'No authorization value';
    } catch(e) {
        console.log(e);
        return unauthorizedResponse(res)
    }
    return next();
})

// Gets all Lists for a given user
listNode.get('/lists', getAllLists);

// Creats a List for a given user
listNode.route('/list')
    // Get a specific list by name via query
    .get(getListByQueryName)
    .post(createNewList)

//Routes for a given list
listNode.route('/list/:listid')
    .get(getListByRouteId)
    // Update a specific list by id as a route parameter
    .post(updateList)
    // Delete a specific list by id
    .delete(deleteList)

// Nested route for a given list's id
listNode.use('/list/:listid/items', itemNode);

export default listNode;