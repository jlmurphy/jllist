import { 
    Router,  
    NextFunction, 
    Request, 
    Response 
} from 'express';
import { 
    deleteSingleItemById, 
    getAllItems, 
    getSingleItemById, 
    insertNewItemToList, 
    toggleCheckedForeSingleItemById, 
    updateSingleItemById 
} from '../handlers';

const itemNode = Router({ mergeParams: true });

itemNode.use((req: Request, res: Response, next: NextFunction) => {
    next();
})

itemNode.route('/')
    // Insert new item into list
    .post(insertNewItemToList)
    // Get all items from a list
    .get(getAllItems);

itemNode.route('/:itemid')
    // Get single list item
    .get(getSingleItemById)
    // Update single item
    .post(updateSingleItemById)
    // Delete single item
    .delete(deleteSingleItemById)

itemNode.get('/:itemid/checked', toggleCheckedForeSingleItemById)

export default itemNode;