import { 
    Router,  
    NextFunction, 
    Request, 
    Response 
} from 'express';
import { okResponse, serverErrorResponse } from '../../utils/http-methods';
import engine from '../../db';

export const getAllUsers = async (req: Request, res: Response) => {
    try {
        const result = await engine.getAllUsers();
        return okResponse(req, res, result);
    } catch(e) {
        return serverErrorResponse(res, e, 2)
    }
}