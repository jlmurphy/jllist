import { 
    Router, 
} from 'express';
import { getAllUsers } from './handlers';

const userNode = Router({ mergeParams: true })

/**
 * Convenience handler to return all users seeded
 */
userNode.get('/', getAllUsers);

export default userNode;
