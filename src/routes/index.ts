import listNode from './lists';
import { 
    Router,  
    NextFunction, 
    Request, 
    Response 
} from 'express';
import { 
    notFoundResponse, 
    unauthorizedResponse 
} from '../utils/http-methods';
import userNode from './users';

const api = Router();


// We keep this above the check since we want to query all users
api.use('/users', userNode);

/**
 * This acts as a mock authorization/authentication middleware
 */
api.use((req: Request, res: Response, next: NextFunction) => {
    const {
        authorization
    } = req.headers;
    try {
        if (!authorization) throw 'No authorization value';
        if (typeof authorization !== 'string') throw 'authorization value not a string';
        
        // This could be simplified..
        const bearer = authorization.split(': ');
        if (bearer.length !== 2) throw 'Invalid authorization key length or format';
        req.headers.userId = bearer[1];
    } catch(e) {
        console.log(e);
        return unauthorizedResponse(res)
    }
    return next();
})

api.use(listNode)

api.use((req: Request, res: Response, next: NextFunction) => {
    return notFoundResponse(res);
})

export default api;