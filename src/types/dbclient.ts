import { 
    IDBList, 
    IAllUserLists, 
    INewDBList, 
    IUpdateDBListFields, 
    INewDBListItem, 
    IDBListItem, 
    IUpdateDBListItem 
} from './List';
import User from './User';

type AsyncFunction <A,O> = (...args: A[]) => Promise<O> 

type connectFunction = AsyncFunction<[null], any>;

interface IListDBClient {
    connect(): Promise<void>,
    getAllUsers(): Promise<User[]>,
    getList(user: string, listId: string): Promise<IDBList>
    getListByName(user: string, listName: string): Promise<IDBList>
    getAllLists(user: string): Promise<IAllUserLists>
    createNewList(user: string, list: INewDBList): Promise<IDBList>
    deleteList(user: string, list: string): Promise<void>
    updateList(user: string, list: string, content: IUpdateDBListFields): Promise<IDBList>
    insertItemToList(user: string, list: string, item: INewDBListItem): Promise<IDBList>
    getAllItems(user: string, list: string): Promise<IDBListItem[]>
    getItemById(user: string, list: string, item: string): Promise<IDBListItem>
    updateItemById(user: string, list: string, item: string, content: IUpdateDBListItem): Promise<IDBList>
    toggleCheckedOfItemById(user: string, list: string, item: string): Promise<IDBList>
    deleteItemById(user: string, list: string, item: string): Promise<IDBList>
}

export default IListDBClient;