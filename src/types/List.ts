import { 
    ObjectID, 
    ObjectId 
} from "mongodb";


export interface INewDBListItem {
    _id?: string | ObjectID;
    name: string;
    desciption?: string;
    checked: number
}

export interface IDBListItem {
    _id: string | ObjectID;
    name: string;
    desciption?: string;
    checked: number
}

export interface IUpdateDBListItem {
    _id?: string;
    name?: string;
    description?: string;
    checked?: number
}

export interface INewDBList {
    _id?: string | ObjectId;
    name?: string;
    description?: string;
    items?: IDBListItem[];
}

export interface IUpdateDBListFields {
    name?: string;
    description?: string;
    items?: any[]
}

export interface IDBList {
    _id: string | ObjectId;
    name?: string;
    description?: string;
    items?: IDBListItem[];
}

export interface IAllDBLists {
    lists: IDBList[];
}

export interface ListQueryObject {
    _id?: string | ObjectId;
    name?: string;
}

export type IAllUserLists = Array<IDBList>