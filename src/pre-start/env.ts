import path from 'path';
import dotenv from 'dotenv';

/**
 * We'll just load both .env (which is for the published environment)
 * and .env.dev (which is the dev environment), for simplicity
 */
(() => {
    try {
        dotenv.config({
            path: path.join(process.cwd(), `.env`),
        })
        dotenv.config({
            path: path.join(process.cwd(), `.env.dev`),
        })
    } catch(e) {
        console.log(e);
        process.exit(1);
    }
})();